# - Try to find gphoto2
# Once done this will define
#  GPHOTO2_FOUND - System has gphoto2
#  GPHOTO2_INCLUDE_DIRS - The gphoto2 include directories
#  GPHOTO2_LIBRARIES - The libraries needed to use gphoto2
#  GPHOTO2_DEFINITIONS - Compiler switches required for using gphoto2

FIND_PATH(GPHOTO2_INCLUDE_DIR gphoto2-version.h
  HINTS /usr/include/gphoto2 )

FIND_LIBRARY(GPHOTO2_LIBRARY NAMES gphoto2
  HINTS /usr/lib64 )

FIND_LIBRARY(GPHOTO2_PORT_LIBRARY NAMES gphoto2_port
  HINTS /usr/lib64 )

INCLUDE(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set GPHOTO2_FOUND to TRUE
# if all listed variables are TRUE
FIND_PACKAGE_HANDLE_STANDARD_ARGS(GPHOTO2  DEFAULT_MSG
  GPHOTO2_LIBRARY GPHOTO2_PORT_LIBRARY GPHOTO2_INCLUDE_DIR)

MARK_AS_ADVANCED(GPHOTO2_INCLUDE_DIR GPHOTO2_LIBRARY )

SET(GPHOTO2_LIBRARIES ${GPHOTO2_LIBRARY} ${GPHOTO2_PORT_LIBRARY} )
SET(GPHOTO2_INCLUDE_DIRS ${GPHOTO2_INCLUDE_DIR} )

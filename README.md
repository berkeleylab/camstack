# CamStack

Small tool to remotely aquire pictures from a digital camera at varying focal depth and scripts to perform align/resize of pcitrues, and focus stacking.

## Dependencies

- gphoto2
- cmake3 

```
sudo yum install gphoto2 gphoto2-devel cmake
```

- Hugin

Install from https://centos.pkgs.org/7/nux-dextop-x86_64/hugin-2014.0.0-4.el7.nux.x86_64.rpm.html

```
wget http://li.nux.ro/download/nux/dextop/el7/x86_64/nux-dextop-release-0-5.el7.nux.noarch.rpm
sudo rpm -Uvh nux-dextop-release*rpm
sudo yum install hugin
```

## Compatibility

The code to autoamtically take photos is currently tested with:

- Canon EOS M6

Your camera might potentially work as the gphoto2 library supports a broad range, but the way the focus is changed might be camera specific and current code might be incompatible with your camera (although likely easy to adapt).

## How to run

Three distinct steps:

1) Take the photos at varying focal distance, such that there is no gap in focus along the object (the focal distance step size needs to be small enough)
2) As changing the focal distance also changes the total area in the picture, the photos need to be aligned, resized/cropped.
3) Focus stack the images only using the parts of a photo which is in focus, resulting in a final stacked picture which is fully focus at all distances.

### Take pictures

Focus camera on the furthest point you want to have in focus.
All other options for the pictures should also be set before hand (ISO, f-stop, ...)

Either take pictures by hand moving focus step-wise nearer until the closest part to the camera is in focus.

Or run snap tool to do this automated, like this:
```
build/snap -o output -n photo -s 0 -n 10
```

### Align

The order of the pictures matters and the first picture should ideally still have something in focus for the alignment to work.

```
align_image_stack -m -a output/aligned_ $(ls -r1 output/*.cr2) -v 
```

### Focus stack

```
enfuse --gray-projector=l-star --exposure-weight=0 --saturation-weight=0 --contrast-weight=1 --contrast-window-size=5 --hard-mask --output=output/stacked.tif output/aligned_00*.tif
```

#include <iostream>
#include <iomanip>
#include <sstream>
#include <getopt.h>

#include <string.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include <gphoto2/gphoto2-camera.h>

/*
 * Date: Aug 2021
 * Author: Karol Krizka, Timon Heim
 * Description: Takes number of pictures at varying focal
 *   distances using gphoto library.
 */


static int gp_lookup_widget(CameraWidget *widget, const char *key, CameraWidget **child)
{
    int ret;
    ret = gp_widget_get_child_by_name (widget, key, child);
    if (ret < GP_OK)
        ret = gp_widget_get_child_by_label (widget, key, child);
    return ret;
}

static void ctx_error_func (GPContext *context, const char *str, void *data)
{
    std::cerr << "Context Error: "  << str << std::endl;
}

static void ctx_status_func(GPContext *context, const char *str, void *data)
{
    std::cerr << "Context Status: " << str << std::endl;
}

  int c;
void usage(char *argv[]) {
    std::cerr << "Usage: " << argv[0] << "[options]" << std::endl;
    std::cerr << "Command line options: " << std::endl;
    std::cerr << "  -o <string>, --output <string>: select output folder" << std::endl;
    std::cerr << "  -n <string>, --name <string>: select name of files" << std::endl;
    std::cerr << "  -s <int>, --step <int>: select focus step size [0, 1, 2]" << std::endl;
    std::cerr << "  -i <int>, --number <int>: select number of pictures to be taken" << std::endl;
    std::cerr << "  -d, --debug : enable debug output" << std::endl;
    std::cerr << "  -h , --help : shows this" << std::endl;
}

int main(int argc, char* argv[])
{
    std::cout << "Reading command line arguments ..." << std::endl;

    std::string outputFolder = "./";
    std::string name = "capture_";
    int stepSize = 0; // Small steps to NEAR
    int numberOfPictures = 10;
    bool debug = false;

    int c;
    while (1) {
        int option_index = 0;
        static struct option long_options[] = {
            {"output",   required_argument, 0,  'o' },
            {"name",     required_argument, 0,  'n' },
            {"step",     required_argument, 0,  's' },
            {"number",   required_argument, 0,  'i' },
            {"debug",    no_argument      , 0,  'd' },
            {"help",     no_argument      , 0,  'h' },
            {0,          0,                 0,  0 }
        };

        c = getopt_long(argc, argv, "o:n:s:i:dh", long_options, &option_index);
        if (c == -1)
            break;

        switch (c) {
            case 'o':
                outputFolder = optarg;
                break;
            case 'n':
                name = optarg;
                break;
            case 's':
                stepSize = std::stoi(optarg);
                break;
            case 'i':
                numberOfPictures = std::stoi(optarg);
                break;
            case 'd':
                debug = true;
                break;
            case 'h':
                usage(argv);
                return 1;
            default:
                std::cerr << "Invalid option '" << c << "' supplied. Aborting." << std::endl;
                std::cerr << std::endl;
                usage(argv);
                return 1;
        }
    }

    if (stepSize > 2 || stepSize < 0) {
        std::cerr << "Step size hould be [0, 1, 2]. If you think you know what you are doing by pass this ..." << std::endl;
        return 1;
    }

    std::cout << "Settings are:" << std::endl;
    std::cout << "  Output folder: " << outputFolder << std::endl;
    std::cout << "  Output name: " << name << std::endl;
    std::cout << "  Step size: " << stepSize << std::endl;
    std::cout << "  Number of Pictures: " << numberOfPictures << std::endl;

    std::cout << std::endl;
    std::cout << "Opening camera connection ... " << std::endl;

    int ret;

    // Create context and camera
    GPContext *context;
    context = gp_context_new();

    gp_context_set_error_func (context, ctx_error_func , NULL);
    gp_context_set_status_func(context, ctx_status_func, NULL);

    Camera *camera=nullptr;
    gp_camera_new(&camera);

    ret=gp_camera_init(camera, context);
    if (ret < GP_OK)
    {
        std::cerr << "No camera auto detected. " << ret << std::endl;
        gp_camera_free (camera);
        return 0;
    }

    std::cout << " ... success!" << std::endl << std::endl;

    //
    // Capture to file
    std::stringstream ss;
    for(int i=0; i<numberOfPictures; i++) {

        ss.str("");
        ss << (outputFolder + "/" + name + "_") << std::setw(5) << std::setfill('0') << i << ".cr2";
        
        std::cout << "Capturing photo #" << i << " to " << ss.str() << std::endl;

        //Capture image
        // CameraFile *file=nullptr;

        // int fd = open(ss.str().c_str(), O_CREAT | O_WRONLY, 0644);
        // ret = gp_file_new_from_fd(&file, fd);
        // ret = gp_camera_capture_preview(camera, file, context);
        // gp_file_free(file);

        //Capture image
        CameraFile *file=nullptr;
        CameraFilePath camera_file_path;

        if (debug)
            std::cout << "Snap photo" << std::endl;

        ret = gp_camera_capture(camera, GP_CAPTURE_IMAGE, &camera_file_path, context);
        if(ret!=GP_OK)
            std::cout << "Error with gp_camera_capture: " << ret << std::endl;

        if (debug)
            std::cout << "Transfer to file" << std::endl;

        int fd = open(ss.str().c_str(), O_CREAT | O_WRONLY, 0644);
        ret = gp_file_new_from_fd(&file, fd);
        if(ret!=GP_OK)
            std::cout << "Error with gp_file_new_from_fd: " << ret << std::endl;      
        ret = gp_camera_file_get(camera, camera_file_path.folder, camera_file_path.name, GP_FILE_TYPE_NORMAL, file, context);
        if(ret!=GP_OK)
            std::cout << "Error with gp_camera_file_get: " << ret << std::endl;            
        gp_file_free(file);

        if (debug)
            std::cout << "Transfer to file" << std::endl;

        ret = gp_camera_file_delete(camera, camera_file_path.folder, camera_file_path.name, context);
        if(ret!=GP_OK)
            std::cout << "Error with gp_camera_file_delete: " << ret << std::endl;            

        sleep(1);
        
        if (debug)
            std::cout << "Change focus" << std::endl;
        // Change focus
        // Find the setting
        CameraWidget *widget=nullptr;
        CameraWidget *child=nullptr;
        gp_camera_get_config(camera, &widget, context);
        gp_lookup_widget(widget, "manualfocusdrive", &child);

        // Set the choice corresponding to -3 to 3, but starting at 0
        char *mval=nullptr;
        ret=gp_widget_get_value(child,&mval);
        if(ret!=GP_OK)
            std::cout << "Error with gp_widget_get_value: " << ret << std::endl;

        //Choice: 0 Near 1
        //Choice: 1 Near 2
        //Choice: 2 Near 3
        //Choice: 3 None
        //Choice: 4 Far 1
        //Choice: 5 Far 2
        //Choice: 6 Far 3
        ret=gp_widget_get_choice(child, stepSize,(const char**)&mval);
        
        if(ret!=GP_OK)
            std::cout << "Error with gp_widget_get_choice: " << ret << std::endl;

        // Set the setting
        ret=gp_widget_set_value(child, mval);
        if(ret!=GP_OK)
            std::cout << "Error with gp_widget_set_value" << std::endl;

        ret=gp_camera_set_config(camera, widget, context);
        if(ret!=GP_OK)
            std::cout << "Error with gp_camera_set_config" << std::endl;

        gp_widget_free(widget);
        //usleep(10000);
        sleep(2);
    }
        
    std::cout << "Done!" << std::endl;
    std::cout << "Backing up focus ..." << std::endl;

    for(int i=0; i<numberOfPictures; i++) {
        if (debug)
            std::cout << "Back up focus" << std::endl;
        // Change focus
        // Find the setting
        CameraWidget *widget=nullptr;
        CameraWidget *child=nullptr;
        gp_camera_get_config(camera, &widget, context);
        gp_lookup_widget(widget, "manualfocusdrive", &child);

        // Set the choice corresponding to -3 to 3, but starting at 0
        char *mval=nullptr;
        ret=gp_widget_get_value(child,&mval);
        if(ret!=GP_OK)
            std::cout << "Error with gp_widget_get_value: " << ret << std::endl;

        //Choice: 0 Near 1
        //Choice: 1 Near 2
        //Choice: 2 Near 3
        //Choice: 3 None
        //Choice: 4 Far 1
        //Choice: 5 Far 2
        //Choice: 6 Far 3
        ret=gp_widget_get_choice(child, stepSize+4,(const char**)&mval);
        
        if(ret!=GP_OK)
            std::cout << "Error with gp_widget_get_choice: " << ret << std::endl;

        // Set the setting
        ret=gp_widget_set_value(child, mval);
        if(ret!=GP_OK)
            std::cout << "Error with gp_widget_set_value" << std::endl;

        ret=gp_camera_set_config(camera, widget, context);
        if(ret!=GP_OK)
            std::cout << "Error with gp_camera_set_config" << std::endl;

        gp_widget_free(widget);
        
        sleep(2);
    }

    std::cout << " ... done!" << std::endl << std::endl;

    //
    // Cleanup
    gp_camera_exit(camera,context);
    
    std::cout << "All done, exiting!" << std::endl;

    return 0;
}
